[![Build status](https://ci.appveyor.com/api/projects/status/vp5mt2dc86hnp8lf?svg=true)](https://ci.appveyor.com/project/ndk/fmsel)

## Quick Start
Prerequisites:
- Git
- CMake 3.10+

```
> git clone git@gitlab.com:darklegacy/fmsel.git --recursive
> cd fmsel
```

### VS 2008
```
> solution.cmd
```

### VS 2017
File->Open->Folder... :)
