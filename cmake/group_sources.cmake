macro(group_sources dir)
  file(GLOB_RECURSE elements RELATIVE ${dir} *.h *.hpp *.c *.cpp *.cc *.cxx)

  foreach(element ${elements})
    get_filename_component(element_name ${element} NAME)
    get_filename_component(element_dir ${element} DIRECTORY)

    if (${element_dir} STREQUAL "")
      source_group("\\" FILES ${dir}/${element})
    else()
      string(REPLACE "/" "\\" group_name ${element_dir})
      source_group("${group_name}" FILES ${dir}/${element})
    endif()
  endforeach()
endmacro()

set_property(GLOBAL PROPERTY USE_FOLDERS ON)
