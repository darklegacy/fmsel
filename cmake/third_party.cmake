set(THIRD_PARTY_DIR ${CMAKE_SOURCE_DIR}/third_party)

set(BZIP2_DIR   ${THIRD_PARTY_DIR}/bzip2)
set(ZLIB_DIR    ${THIRD_PARTY_DIR}/zlib)
set(MINIZIP_DIR ${THIRD_PARTY_DIR}/zlib/contrib/minizip)
set(LIB7ZIP_DIR ${THIRD_PARTY_DIR}/lib7zip)
set(LIBPNG_DIR  ${THIRD_PARTY_DIR}/libpng)
set(LIBJPEG_DIR ${THIRD_PARTY_DIR}/libjpeg-turbo)
set(OGG_DIR     ${THIRD_PARTY_DIR}/ogg)
set(VORBIS_DIR  ${THIRD_PARTY_DIR}/vorbis)
set(FLTK_DIR    ${THIRD_PARTY_DIR}/fltk)

###############################################################################
set(BZIP2_FOUND)
set(BZIP2_LIBRARY libbz2)
set(BZIP2_LIBRARIES ${BZIP2_LIBRARY})
set(BZIP2_INCLUDE_DIRS
  ${BZIP2_DIR}
)
set(BZIP2_INCLUDE_DIR ${BZIP2_INCLUDE_DIRS} CACHE PATH "")

###############################################################################
set(ZLIB_FOUND)
set(ZLIB_LIBRARY zlibstatic)
set(ZLIB_LIBRARIES ${ZLIB_LIBRARY})
set(ZLIB_INCLUDE_DIRS
  ${ZLIB_DIR}
  ${CMAKE_BINARY_DIR}/third_party/zlib
)
set(ZLIB_INCLUDE_DIR ${ZLIB_INCLUDE_DIRS} CACHE PATH "")

###############################################################################
set(MINIZIP_FOUND)
set(MINIZIP_LIBRARY minizip)
set(MINIZIP_LIBRARIES ${MINIZIP_LIBRARY})
set(MINIZIP_INCLUDE_DIRS
  ${MINIZIP_DIR}/..
)
set(MINIZIP_INCLUDE_DIR ${MINIZIP_INCLUDE_DIRS} CACHE PATH "")

###############################################################################
set(LIB7ZIP_FOUND)
set(LIB7ZIP_LIBRARY lib7zip)
set(LIB7ZIP_LIBRARIES ${LIB7ZIP_LIBRARY})
set(LIB7ZIP_INCLUDE_DIRS
  ${LIB7ZIP_DIR}
)
set(LIB7ZIP_INCLUDE_DIR ${LIB7ZIP_INCLUDE_DIRS} CACHE PATH "")

###############################################################################
set(PNG_FOUND ON)
set(PNG_LIBRARY png_static)
set(PNG_LIBRARIES ${PNG_LIBRARY})
set(PNG_INCLUDE_DIRS
  ${LIBPNG_DIR}
  ${CMAKE_CURRENT_BINARY_DIR}/third_party/libpng) # Libpng generated pnglibconf.h file
set(PNG_INCLUDE_DIR ${PNG_INCLUDE_DIRS} CACHE PATH "")
set(PNG_PNG_INCLUDE_DIR ${PNG_INCLUDE_DIRS} CACHE PATH "")

###############################################################################
set(JPEG_FOUND ON)
set(JPEG_INCLUDE_DIR ${LIBJPEG_DIR})
set(JPEG_LIBRARY jpeg-static CACHE FILEPATH "")
set(JPEG_LIBRARIES ${JPEG_LIBRARY})
set(JPEG_INCLUDE_DIRS
  ${JPEG_INCLUDE_DIR}
  ${CMAKE_BINARY_DIR}/third_party/libjpeg-turbo
)
set(JPEG_INCLUDE_DIR ${JPEG_INCLUDE_DIRS} CACHE PATH "")

###############################################################################
set(FLTK_FOUND)
set(FLTK_LIBRARY fltk fltk_images)
set(FLTK_LIBRARIES ${FLTK_LIBRARY})
set(FLTK_INCLUDE_DIRS
  ${FLTK_DIR}
)
set(FLTK_INCLUDE_DIR ${FLTK_INCLUDE_DIRS} CACHE PATH "")

###############################################################################
set(OGG_FOUND)
set(OGG_LIBRARY ogg)
set(OGG_LIBRARIES ${OGG_LIBRARY})
set(OGG_INCLUDE_DIRS
  ${OGG_DIR}
)
set(OGG_INCLUDE_DIR ${OGG_INCLUDE_DIRS} CACHE PATH "")

###############################################################################
set(VORBIS_FOUND)
set(VORBIS_LIBRARY vorbis vorbisfile)
set(VORBIS_LIBRARIES ${VORBIS_LIBRARY})
set(VORBIS_INCLUDE_DIRS
  ${VORBIS_DIR}/include
)
set(VORBIS_INCLUDE_DIR ${VORBIS_INCLUDE_DIRS} CACHE PATH "")

###############################################################################
function(get_all_targets _result _dir)
	get_property(_subdirs DIRECTORY "${_dir}" PROPERTY SUBDIRECTORIES)
	foreach(_subdir IN LISTS _subdirs)
		get_all_targets(${_result} "${_subdir}")
	endforeach()
	get_property(_sub_targets DIRECTORY "${_dir}" PROPERTY BUILDSYSTEM_TARGETS)
	set(${_result} ${${_result}} ${_sub_targets} PARENT_SCOPE)
endfunction()

function(add_subdirectory_with_folder _folder_name _folder)
	add_subdirectory(${_folder} ${ARGN})
	get_all_targets(_targets "${_folder}")
	foreach(_target IN LISTS _targets)
		set_target_properties(
			${_target}
			PROPERTIES FOLDER "${_folder_name}"
		)
	endforeach()
endfunction()

add_subdirectory_with_folder(third_party third_party EXCLUDE_FROM_ALL)
